-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 05 jan. 2020 à 01:45
-- Version du serveur :  10.3.14-MariaDB
-- Version de PHP :  7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `test`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `body` text NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`id`, `author_id`, `title`, `body`, `create_at`, `update_at`) VALUES
(8, 1, 'teztez', 'tzetztzt', '2019-12-26 16:37:58', '2019-12-26 16:37:58'),
(10, 1, 'wow', 'it works', '2019-12-28 22:17:29', '2019-12-28 22:17:29');

-- --------------------------------------------------------

--
-- Structure de la table `articles_category`
--

DROP TABLE IF EXISTS `articles_category`;
CREATE TABLE IF NOT EXISTS `articles_category` (
  `articles_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`articles_id`,`category_id`),
  KEY `category_id` (`category_id`),
  KEY `articles_id` (`articles_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `articles_category`
--

INSERT INTO `articles_category` (`articles_id`, `category_id`) VALUES
(8, 1),
(8, 2);

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Doc'),
(2, 'Fantasy');

-- --------------------------------------------------------

--
-- Structure de la table `group_roles`
--

DROP TABLE IF EXISTS `group_roles`;
CREATE TABLE IF NOT EXISTS `group_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `group_roles`
--

INSERT INTO `group_roles` (`id`, `name`) VALUES
(1, 'test');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(150) NOT NULL,
  `firstname` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `users_group_id_idfk1` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `firstname`, `lastname`, `group_id`, `create_at`, `update_at`) VALUES
(1, 'admin', '67b36bda727eaeb1c8ca1919c44b6bcbbfde6fcc7d7a5366921d516d69592a42', 'admin@litefram.local', 'LiteFrame', 'Admin', 1, '2019-11-21 15:24:16', '2019-12-18 11:35:37');

-- --------------------------------------------------------

--
-- Structure de la table `users_group`
--

DROP TABLE IF EXISTS `users_group`;
CREATE TABLE IF NOT EXISTS `users_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users_group`
--

INSERT INTO `users_group` (`id`, `name`) VALUES
(1, 'system'),
(2, 'client');

-- --------------------------------------------------------

--
-- Structure de la table `users_group_has_roles`
--

DROP TABLE IF EXISTS `users_group_has_roles`;
CREATE TABLE IF NOT EXISTS `users_group_has_roles` (
  `group_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `role_link` tinyint(1) NOT NULL DEFAULT 1,
  `role_add` tinyint(1) NOT NULL DEFAULT 1,
  `role_update` tinyint(1) NOT NULL DEFAULT 1,
  `role_delete` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`group_id`,`role_id`),
  KEY `group_has_role_pivote_idfk2` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users_group_has_roles`
--

INSERT INTO `users_group_has_roles` (`group_id`, `role_id`, `role_link`, `role_add`, `role_update`, `role_delete`) VALUES
(1, 1, 0, 1, 1, 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `article_authot_idfk1` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `articles_category`
--
ALTER TABLE `articles_category`
  ADD CONSTRAINT `articlecategory_idfk1` FOREIGN KEY (`articles_id`) REFERENCES `articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `articlecategory_idfk2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_group_id_idfk1` FOREIGN KEY (`group_id`) REFERENCES `users_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `users_group_has_roles`
--
ALTER TABLE `users_group_has_roles`
  ADD CONSTRAINT `group_has_role_pivote_idfk1` FOREIGN KEY (`group_id`) REFERENCES `users_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `group_has_role_pivote_idfk2` FOREIGN KEY (`role_id`) REFERENCES `group_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
