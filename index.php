<?php
ob_start();
// ----------------- Custome Setup -------------
define("_LFW_DEV_MODE",true); // developer mode
define("LFW_ENCRYPT_KEY","LFWSECRET");

define("_LFW_ROOT_PATH",dirname(__FILE__)); // ROOT PATH

define("_LFW_Sys_PATH",_LFW_ROOT_PATH."/SysData"); // PATH TO SysData 
define("_LFW_Log_PATH",_LFW_ROOT_PATH."/ErrorLogs"); // PATH TO ErrorLogs 

define("_LFW_Conf_PATH",_LFW_ROOT_PATH."/Conf"); // PATH TO Conf (global config) 

define("_LFW_Data_PATH",_LFW_ROOT_PATH."/Data"); // PATH TO Data (Apps & Common) 

define("_LFW_APP_PATH",_LFW_Data_PATH."/Apps"); // PATH to Data/Apps

ini_set('max_execution_time', 120);

require_once(_LFW_Sys_PATH."/init/errorhandler.php");
require_once(_LFW_Conf_PATH."/config.php");
require_once(_LFW_Sys_PATH."/init/database.php");

require_once(_LFW_Sys_PATH."/init/system.php");
require_once(_LFW_Sys_PATH."/init/compload.php");
require_once(_LFW_Sys_PATH."/init/autoloader.php");
require_once(_LFW_Sys_PATH."/libs/vendor/autoload.php");

use Lfw\ErrorHandler;
use Lfw\system\Loader;
use Lfw\Core\Engines\EngineTwig;
use Lfw\helpers\Tools;
use Lfw\helpers\Url;
use Lfw\helpers\Session;


$TwigEngine = new EngineTwig();

$currenturl = Url::GetRequestPage();

$App = Loader::UseSubApp($currenturl);
if(!$App)
	ErrorHandler::View("501");

if(!$App['enabled'])
	ErrorHandler::View("404");

Define("_LFW_CURRENT_APP_",$App['folder']);
Define("_LFW_CURRENT_APP_ROOT_",_LFW_APP_PATH."/"._LFW_CURRENT_APP_);

if($App['twig'])
{
	$paths = [];
	if(is_dir(_LFW_CURRENT_APP_ROOT_."/view") )
	{
		$TwigEngine->SetLoader("file",_LFW_CURRENT_APP_ROOT_."/view");
		$TwigEngine->AddPath("assets",_LFW_ROOT_PATH."/WebRoot");
		$TwigEngine->SetEnvironment();
	}
}

Session::start();

$filtredurl = (isset($App['prefix']) ? preg_replace("#^/$App[prefix]/?#", null, $currenturl) : $currenturl);

$data = Loader::LoadUrlManager( $filtredurl  );
if( $data && isset($data['ctrl'],$data['action']) )
{
	$obj = Loader::LoadCtrl($data['ctrl'] ,( isset($App['prefix']) ? $App['prefix'] : null ), $TwigEngine);
	if(!$obj)
		ErrorHandler::View("501");
	else
	{
		$buff = $obj->_Run_( $data['action'] , isset($data['variables']) ? $data['variables'] : [] );
		if(!$buff)
			ErrorHandler::View("403");
		else
			die($buff);
	}
}
else
	ErrorHandler::View("404");

ob_end_clean();
?>
