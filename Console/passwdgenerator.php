<?php
define("LFW_ENCRYPT_KEY","LFWSECRET");
Function HashStr($Input , $key=LFW_ENCRYPT_KEY , $base=false)
{
	$String = trim($Input);

	$strhash = hash_hmac('sha256', $String  , $key );
	if($base)
		return base_convert($strhash,16,$base);

	return $strhash;
}

if(isset($_GET["pass"]))
	echo HashStr($_GET['pass']);

?>