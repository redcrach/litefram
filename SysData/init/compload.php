<?php

function LoadComponent($boots,$root)
{
	foreach($boots as $boot)	
	{
		if(file_exists(_LFW_Sys_PATH."/components/{$root}/{$boot}.php"))
			require_once(_LFW_Sys_PATH."/components/{$root}/{$boot}.php");
		else
			ErrorHandler::Throw("$boot don't exist!");
	}
}

$boots = [
		"layout"=>[
					"ctrl",
				],
		"dbf"=>[
					"dbforge",
					"queryforger"
				],
		"orm"=>[
					"model",
					"forger",
					"relation"
				],
		"core"=>[
					"render",
					"authenticator"
				],
		"helpers"=>[
					"tools",
					"web",
					"io",
				],
				
		];


foreach($boots as $type=>$boot)
	LoadComponent($boot,$type);


?>