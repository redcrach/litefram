<?php
namespace LFW;

ini_set ('error_log',_LFW_Log_PATH."/runtime.log");
ini_set ('log_errors', _LFW_DEV_MODE);
ini_set ('display_errors', _LFW_DEV_MODE);
ini_set ('display_startup_errors', _LFW_DEV_MODE);

Class ErrorHandler
{


	Static function View($code,$data=null)
	{
		if(file_exists(_LFW_Sys_PATH."/views/$code.html"))
		{
			$content = file_get_contents(_LFW_Sys_PATH."/views/$code.html");
			$content = str_replace("[[data]]",$data,$content);
			die($content);
		}
		else
			self::Throw("Incorrect Error Code $code");
	}

	Static function Throw($msg,$lines=[])
	{
		$logmsg = $msg;

		if(!empty($lines))
		{
			$logmsg.=",";
			foreach($lines as $trace)
				if(isset($trace['line'],$trace['file']))
					$logmsg.= " Line $trace[line] in $trace[file]  ,";
			
			
			$logmsg=substr($logmsg,0,-1);
		}

		if(_LFW_DEV_MODE)
			die($logmsg);
		else
		{
			file_put_contents(_LFW_Log_PATH."/systemlog.log",$logmsg);
			exit();
		}
	}
}

?>