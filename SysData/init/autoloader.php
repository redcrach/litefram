<?php

spl_autoload_register(function ($ClassName) 
{
	
	if(!Class_exists($ClassName,false))
	{
		$Filename="";
		$matches =[];
		if(preg_match("#^App\\\([a-zA-Z0-9]+)\\\(controller|model)\\\([a-zA-Z0-9]+)$#i",strtolower($ClassName),$matches))
			$Filename = _LFW_APP_PATH."/{$matches[1]}/{$matches[2]}/{$matches[3]}.php";

		if(preg_match("#^Common\\\([a-zA-Z0-9]+)$#i",strtolower($ClassName),$matches))
			$Filename = _LFW_Data_PATH."/Common/{$matches[1]}.php";
			
		
		if (!empty($Filename) && file_exists($Filename))
			require_once $Filename;
		
	}

});

?>