<?php
namespace Lfw\System;
use Lfw\ErrorHandle;

Class Loader
{
	Static Public function Include($file,$root=_LFW_ROOT_PATH)
	{
		if(file_exists("$root/$file"))
			return require_once("$root/$file");
		else
			ErrorHandler::View("501");
	}

	Static Public function UseSubApp($url)
	{
		$default_settings = [
								"twig"=>true,
								"enabled"=>true
							];

		$AppsRoute = Loader::Include("appmanager.php",_LFW_Conf_PATH);
		$fallback = [];
		foreach($AppsRoute as $appname => $appdata)
		{
			if(!is_dir(_LFW_APP_PATH."/$appname") )
				continue;
			$appdata['folder'] = $appname;
			if(isset($appdata['prefix']) && $appdata['prefix'] != null )
			{
				if(preg_match("#^/".preg_quote($appdata['prefix'])."#i", $url))
					return array_merge( $default_settings , $appdata);
			}
			else
				$fallback = $appdata;
		}

		if(!empty($fallback))
			return array_merge( $default_settings , $fallback);
		else
			return false;
	}

	Static function LoadUrlManager($url)
	{
		$AppsRoute = Loader::Include("urlmanager.php",_LFW_CURRENT_APP_ROOT_."/config");

		foreach($AppsRoute as $routingdata )
		{
			$triggerurl = isset($routingdata['url']) ? $routingdata['url'] : NULL;
			$matches = [];
			
			// ****** Ctrl & Action & variables

			preg_match_all("#<(@|%|!|:\w+) (\[/[wdnfa]\]|\{(,?\w+)+\})>#",$triggerurl,$matches);

			$smartregexlist = [
								"/w" => "(\w+)", // word alphanumeric 
								"/d" => "(\d+)", // decimal (numeric)
								"/n" => "([a-zA-Z]+)", // name ( [a-zA-Z] ) 
								"/f" => "([^\.]+)", // everything except .
								"/a" => "(\S+)", // everything except spaces 
							];

			$exp = preg_quote($triggerurl);
			$DynamicCtrlIndex = -1;
			$DynamicActionIndex = -1;
			$DynamicVariables = [];

			for($i=0;!empty($matches[0]) && $i<count($matches[0]);$i++)
			{
				$buff = $matches[0][$i];
				$dynamicexp = preg_quote($buff);

				// check type if ctrl , action , or a variable declaration

				switch( strtolower($matches[1][$i]) )
				{
					case "@":
						$DynamicCtrlIndex = -1;
						$DynamicActionIndex = -1;
						$DynamicVariables = [];
					break;

					case "%":
						$DynamicCtrlIndex = $i;
					break;

					case "!":
						$DynamicActionIndex = $i;
					break;

					default:
						$DynamicVariables[] = substr( strtolower($matches[1][$i]) , 1);
					break;
				}

				// generate regex to be replaced 
				if( strpos($matches[2][$i] , "[") === 0 )
				{
					$dynamicregex = str_replace(["[","]"],null ,  $matches[2][$i]);
					if(isset($smartregexlist[ $dynamicregex ]) )
						$dynamicregex = $smartregexlist[ $dynamicregex ];
					else
						$dynamicregex = $smartregexlist[ $dynamicregex ];
				}
				elseif( strpos($matches[2][$i] , "{") === 0 )
				{
					$dynamicregex = str_replace(["{","}"] ,null ,  $matches[2][$i]);
					$dynamicregex = str_replace(",","|", preg_quote($dynamicregex) );
					$dynamicregex = "($dynamicregex)";
				}
				else
					return false;

				// replace the dynamic expression with the regex 

				$exp = str_replace($dynamicexp , $dynamicregex , $exp);
			}

			if( preg_match("#^/?$exp$#i",$url,$fmatch) )
			{
				if(!empty($fmatch))
				{
					
					array_shift($fmatch);
					
					if($DynamicCtrlIndex!=-1)
					{
						$routingdata["ctrl"] = $fmatch[ $DynamicCtrlIndex ];
						unset($fmatch[ $DynamicCtrlIndex ]);
					}

					if($DynamicActionIndex!=-1)
					{
						$routingdata["action"] = $fmatch[ $DynamicActionIndex ];
						unset($fmatch[ $DynamicActionIndex ]);
					}

					if(!empty($DynamicVariables) && !empty($fmatch))
						$routingdata['variables'] = array_combine($DynamicVariables , $fmatch);
				}

				return $routingdata;
			}
			
		

		}
		return false;
	}

	Static Function LoadCtrl($ctrlname ,$prefix, &$TwigTemplate )
	{
		if(!file_exists(_LFW_CURRENT_APP_ROOT_."/controller/{$ctrlname}.php"))
			return false;
		$app = _LFW_CURRENT_APP_;
		$className = "App\\{$app}\\controller\\{$ctrlname}";
		if(!class_exists($className,false))
		{
			$obj=new $className($TwigTemplate,$prefix);
			return $obj;
		}
		else
			return false;
	}
}

?>