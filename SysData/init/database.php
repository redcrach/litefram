<?php

namespace LFW\Database;

use LFW\Config;
use LFW\ErrorHandler;
use \PDO;
use \PDOException;
use \Exception;

Class rawquery
{
	Private $query;

	function __construct($q )
	{
		$this->query = $q;		
	}

	public function getquery()
	{
		return $this->query;
	}

};

Class DB 
{
	Static Private $Database=false;

	Static Private $opentrans=false;

	Static Public function connect()
	{
		try
		{
			self::$Database = new PDO("mysql:host=".Config\DB_Data['host'].";dbname=".Config\DB_Data['db'], Config\DB_Data["user"],Config\DB_Data['pass']);
			self::$Database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			self::$Database->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			self::$Database->query('SET NAMES utf8');
		}
		catch (Exception $e)
		{
			ErrorHandler::Throw('Erreur : ' . $e->getMessage());
		}
	}

	Static Public function GetConnection()
	{
		return self::$Database;
	}

	Static Public function TStart()
	{
		self::Close();
		self::$Database->beginTransaction();
		self::$opentrans = true;
	}

	Static Public function TCommit()
	{
		if(self::$opentrans)
			self::$Database->commit();
		self::$opentrans = false;
	}

	Static Public function TRollback()
	{
		if(self::$opentrans)
			self::$Database->rollBack();
		self::$opentrans = false;
	}

	Static Public function Close()
	{
		if(self::$opentrans)
			self::$Database->rollBack();
		self::$opentrans = false;
	}

	Static Public function LastInsert()
	{
		return self::$Database->lastInsertId();
	}


	Static Public function Query($sql)
	{
		return self::$Database->query($sql);
	}

	Static Public function Prepare($sql,$param=[],$blockerror = false)
	{
		try
		{
			$req = self::$Database->prepare($sql);
			$status=$req->execute($param);
			return new Class(self::$Database,$req,$status){
								private 
									$Database,
									$_reqest = null,
									$status;

								Function __construct($dbh,$req,$status)
								{
									$this->Database = $dbh;
									$this->_reqest = $req;
									$this->_status = $status;
								}

								Public function GetAll()
								{
									if($this->_reqest!==null && $this->_reqest->rowcount()>0)
										return $this->_reqest->fetchAll(PDO::FETCH_ASSOC);
									else
										return [];
								}

								
								Public function Get()
								{
									if($this->_reqest!==null && $this->_reqest->rowcount())
										return $this->_reqest->fetch(PDO::FETCH_ASSOC);
										
									return [];
								}

								Public function Count()
								{
									if($this->_reqest === null)
										return 0;
									return $this->_reqest->rowcount();
								}

								Public function req()
								{
									return $this->_reqest;
								}

								Public function status()
								{
									return $this->_status;
								}
					};

		}
		catch(PDOException $e) 
		{
			 if(!$blockerror)
				ErrorHandler::Throw($e->getMessage(),$e->getTrace());
			return false;
		}
	}	


	Static Public function Columns($table)
	{
		$table = strtolower($table);
		$columns = [];
		$columnsdata = DB::Query( sprintf("DESCRIBE %s;", $table) );
		foreach($columnsdata->fetchall() as $row)
			$columns[] = strtolower($row["Field"]);
		return $columns;
	}

	Static Public function table($tablename)
	{
		try
		{
			$tableslist = DB::Query( "SHOW TABLES" );
			$tablelist = array_map( function($v){
									return $v[0];
								},$tableslist->fetchall());

			if(in_array($tablename, $tablelist))
				return new DBForge($tablename);
			else
				throw new Exception( sprintf("Table '%s' Dosn't Exist! ",$tablename) );
		}
		catch(PDOException $Exception)
		{
			throw new Exception( sprintf("Table '%s' Dosn't Exist! ",$tablename) );
			return false;
		}
	}

	Static	Public function Raw($query)
	{
		return new rawquery($query);
	}
}



DB::connect();

?>