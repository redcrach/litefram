<?php
namespace Lfw\Core\Transactions;
use \Exception;

use LFW\Helpers\Tools;
use LFW\Helpers\Secure;
use LFW\Helpers\Session;
use LFW\Helpers\Cookies;

class Authenticator
{

	Static Function HashSession($input)
	{
		if(is_array($input))
			$data=json_encode($input);
		else
			$data=$input;
		
		return Base64_Encode( Secure::Hash(  trim($data)  ) );  
	}

	Static Function isOpen()
	{
		if( Session::Exist('Identity') )
			return Session::Get('Identity');
		elseif(Cookies::Exist(["Identity","Token","Sign"]) )
		{
			$identity = Cookies::Get("Identity");
			$token = Cookies::Get("Token");
			$sign = Cookies::Get("Sign");
			$Hash = self::HashSession($identity.$token);
			if(hash_equals($Hash,$sign ) )
				return $identity;
			else
				return false;
		}
		else
			return false;
	}

	Static function SetSession($id,$parmanent=false)
	{
		Session::Create("Identity",$id);
		if($parmanent)
		{
			$token = Tools::RandString(10);
			$sign = self::HashSession($id.$token);
			Cookies::Create("Identity",$id);
			Cookies::Create("Token",$token);
			Cookies::Create("Sign",$sign);
		}

		return true;
	}

	Static function Close()
	{
		if( Session::Exist('Identity') )
			Session::Destroy("Identity");

		if(Cookies::Exist(["Identity","Token","Sign"]) )
		{
			Cookies::Destroy("Identity");
			Cookies::Destroy("Token");
			Cookies::Destroy("Sign");
		}
	}

}


?>