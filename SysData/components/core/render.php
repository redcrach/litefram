<?php
namespace Lfw\Core\Engines;
use \Exception;
use LFW\ErrorHandler;
class EngineTwig
{
	Protected $Twigloader = null;
	Protected $TwigEnv = null;

	Function __construct()
	{

	}

	Public Function SetLoader($type,$arg)
	{
		switch($type)
		{
			case 'file':
				$this->Twigloader = new \Twig\Loader\FilesystemLoader($arg);
			break;

			case 'string':
				$this->Twigloader = new \Twig\Loader\ArrayLoader($arg);
			break;
		}
	}
 

 	Public function SetEnvironment($options=[])
 	{
 		if($this->Twigloader === null)
 			throw new Exception('Loader Undefined');

 		$this->TwigEnv = new \Twig\Environment($this->Twigloader,$options);
 	}


 	Public function AddPath($alias,$path)
 	{
 		if($this->Twigloader === null)
 			throw new Exception('Loader Undefined');
 		if(!is_dir($path))
 			throw new Exception('Illegal Path');
 		$this->Twigloader->addPath($path, $alias);
 	}

 	Public Function GetLoader()
 	{
 		return $this->Twigloader;
 	}

 	Public Function GetEnvironment()
 	{
 		return $this->TwigEnv;
 	}

 	// -------- Add Twig Filter 
 	Public function AddFilter($filtername,$callback,$options=[])
 	{
 		if($this->TwigEnv === null)
 			throw new Exception('Environment Undefined');
 		$filter = new \Twig\TwigFilter($filtername, $callback,$options);
 		$this->TwigEnv->addFilter($filter );

 	}

 	// -------- Add Twig Tests 
 	Public function AddTests($testname,$callback)
 	{
		if($this->TwigEnv === null)
 			throw new Exception('Environment Undefined');
 		
 		$test = new \Twig\TwigTest($testname, $callback);
		$this->TwigEnv->addTest($test);
 	}

 	// --------- Add Global var 
 	Public function AddGlobalvar($varname,&$value)
 	{
 		if($this->TwigEnv === null)
 			throw new Exception('Environment Undefined');
 		$this->TwigEnv->addGlobal($varname, $value );
 	}


 	Public function Render($file ,$params)
 	{
 		if($this->TwigEnv === null)
 			throw new Exception('Environment Undefined');
 		try {
 			
 			return $this->TwigEnv->render("{$file}.twig", $params );	
 		} catch (Exception $e) {
 			ErrorHandler::Throw($e->getMessage());
 		}
 	}
 }


?>