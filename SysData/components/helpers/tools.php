<?php
namespace Lfw\helpers;

class Tools
{
	Static Function RandString($len)
	{
		if($len==false || $len <=0 )
			$len=6;

		if (function_exists('random_bytes'))
			$string = bin2hex(random_bytes( ($len/2) ) );
		else 
			$string = bin2hex(openssl_random_pseudo_bytes( ($len/2) ));
		return $string;
	}
}


?>