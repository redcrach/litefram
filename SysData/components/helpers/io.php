<?php
namespace Lfw\helpers;

class Requester
{
	Static Function GET($key)
	{
		return isset($_GET[$key]) ? $_GET[$key] : false;
	}

	Static function POST($key)
	{
		$postdata=[];
		if(isset($_SERVER["CONTENT_TYPE"]) && strpos($_SERVER["CONTENT_TYPE"],'application/json') )
		{
			$content = trim(file_get_contents("php://input"));
			$postdata = json_decode($content, true);
			if(!is_array($Post))
				$postdata=[];
		}
		else
			$postdata=$_POST;

		return isset($postdata[$key]) ? $postdata[$key] : false;
	}

}

class Session
{
	Static function Start()
	{
		if (ini_get('session.use_cookies') && isset($_COOKIE['PHPSESSID'])) 
		{
            $sessid = $_COOKIE['PHPSESSID'];
            if (!preg_match('/^[a-z0-9]+$/', $sessid))
            {
            	@session_destroy();
            	return false;
            }
        }
		@session_start();
		return true;
	}

	Static function Destroy($id=false)
	{
		if($id)
		{
			if( self::Exist($id) )
				unset($_SESSION[$id]);
		}
		if($id===false)
		{
			@session_destroy();
			@session_start();
		}
	}

	Static Function Create($id,$value)
	{
		$_SESSION[$id]=$value;
	}

	Static Function Get($id)
	{
		if(self::Exist($id))
			return $_SESSION[$id];
		else
			return false;
	}

	Static Function Exist($key)
	{
		if(is_array($key))
		{
			$exist = true;
			foreach($key as $k)
				$exist = $exist && array_key_exists($k, $_SESSION);
			return $exist;
		}
		else
			return array_key_exists($key, $_SESSION);
	}
}

class Cookies
{
	Static function Destroy($id=false)
	{
		if(self::Exist($id))
		{
			unset($_COOKIE[$id]);
			setcookie($id, null, -1, '/');
    		setcookie($id, null, -1, '/');
		}
		
	}

	Static function Create($key,$value,$expire=false,$days=7)
	{
		if($expire)
			$limit= time() + ( 86400 * $days );
		else
			$limit = time() + (86400 * 365);
		
		if( self::Exist($key) )
			$_COOKIE[$key] = $value;
		
		setcookie($key, $value,$limit, "/"); 

	}

	Static function Get($key)
	{
		if(self::Exist($key) ) 
			return $_COOKIE[$key];
		else
			return false;
	}

	Static Function Exist($key)
	{
		if(is_array($key))
		{
			$exist = true;
			foreach($key as $k)
				$exist = $exist && array_key_exists($k, $_COOKIE);
			return $exist;
		}
		else
			return array_key_exists($key, $_COOKIE);
	}
}


class Secure
{
	Static function HtmlSecure($str)
	{
		return htmlentities($str);
	}

	Static Function Hash($Input , $key=LFW_ENCRYPT_KEY , $base=false)
	{
		$String = trim($Input);

		$hash = hash_hmac('sha256', $String  , $key );
		if($base)
			return base_convert($hash,16,$base);

		return $hash;
	}

	
}



?>