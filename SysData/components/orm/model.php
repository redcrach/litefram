<?php
namespace Lfw\Database\ORM;

use LFW\Database\DB;
use LFW\Database\ORM\OrmDBForge;
use LFW\Database\ORM\OrmRelation;
 
use \ReflectionProperty;
use \ReflectionMethod;
use \ReflectionClass;

use \Exception;
use \ReflectionException;
use \DateTime;

use Lfw\Helpers\Url;

Abstract Class Model
{
	Protected Static 
		$primarykey = "id", // the primary key column name,
		$keyisInt = true, // is primary key a INT
		$autoincrement = true, // primary key is auto-increment
		$tablename = null; // table name

	Protected 
		$timestamps = true, // existing of create_at & update_at fields
		$datefields = [], // extra fields that should be handled as DateTime objects
		$_private = [], // Filds that shouldn't be displayed by _ToArray()
		$displaylinks = [], // relationships displayed in _ToArray() 
		$attributes = []; // default values for fields

	Const 
		CREATE_AT = "create_at",
		UPDATE_AT = "update_at";

	Private 
		$publicvars = [],
		$_availablecolumns = [],
		$_modifiedfilds = [];

	function __construct( $datatoload = [])
	{
		$className = get_called_class();

		if(static::$tablename == null)
			static::$tablename = strtolower( basename($className) );

		if(static::$autoincrement && !static::$keyisInt )
	 		throw new Exception( sprintf("PK can't be autoincrement and no-int in ORM model %s",$className) );
	 	
		$forger = new OrmDBForge($className , static::$tablename);

	 	// ----  Setup a orm model ---

	 	$this->_availablecolumns = DB::Columns(static::$tablename);

	 	if( $this->timestamps )
	 	{
	 		$res = array_diff( [static::CREATE_AT , static::UPDATE_AT] , $this->_availablecolumns);
	 		if(!empty($res) )
	 			throw new Exception(sprintf("fields (%s) missing in table %s ",implode(',',$res),static::$tablename));

	 		// add default create_at value 
			$this->publicvars[static::CREATE_AT] = new DateTime();
	 		// add default update_at value 
			$this->publicvars[static::UPDATE_AT] = new DateTime();
	 	}

	 	foreach( $this->attributes as $field => $value )
	 	{
	 		if(!in_array($field, [static::CREATE_AT , static::UPDATE_AT] ))
	 			continue;
	 		if( !$this->attributeValue($field,$value))
	 			throw new Exception(sprintf("field '%s' dosn't exist in table '%s' ",$field,static::$tablename));
	 	}	
	 	
	 	if(!empty($datatoload) && !$this->HydrateByArray($datatoload))
	 		throw new Exception( sprintf("Illegal data for ORM model %s",$className) );
	}

// ********************************************
	Private function attributeValue($field,$value)
	{
		$field = strtolower($field);

		if(!in_array($field , $this->_availablecolumns ))
			return false;

		if(is_bool($value))
			$value = intval($value);
	 	if(in_array($field,$this->datefields) || ($this->timestamps && in_array($field,[static::UPDATE_AT,static::CREATE_AT]) ) )
	 		$this->publicvars[$field] = ( $value instanceof DateTime ) ? $value : (new DateTime($value));
	 	else
	 		$this->publicvars[$field] = $value;
		return true;
	}

	Private function GetAttribute($field)
	{
		$field = strtolower($field);
		if(!in_array($field , $this->_availablecolumns ))
			return false;

		if(!array_key_exists($field , $this->publicvars ))
			return null;

	 	$value = $this->publicvars[$field];
	 	if(is_bool($value))
	 		$value = intval($value);

		return $value;
	}

	Private function HydrateFromDB($id)
	{
		if(!self::IsPK($id))
			return false;
		
		$forger = new OrmDBForge($className , $this->table());
		$data = $folrger->Where($this->getpk() , $id)->Get(true);
		if($data)
			return $this->HydrateByArray($data);
		
		return false;
	}

	Private function HydrateByArray($data)
	{
		if(!is_array($data))
	 		throw new Exception( sprintf("Illegal data array expected in class %s",get_called_class()) );

		foreach($data as $field=>$value)
			$this->attributeValue($field,$value);

		$this->_modifiedfilds = [];

		return !empty($this->publicvars);
	}

	Private function MultiArrayToModel($array)
	{
		$result = [];
		foreach($array as $data)
		{
			try 
			{
				$reflectionObj = new ReflectionClass(get_called_class());
      			$ModelObj = $reflectionObj->newInstanceArgs( [$data] );
    			$result[] = $ModelObj;
			}
			catch (Exception $e) {
				continue;
			}
		}

		return $result;
	}

	Private function TrackModification($field , $oldval,$newval)
	{
    	$this->_modifiedfilds[$field] = [ $oldval,$newval ];
	}

	Private function IsLinkMethod($methodname)
	{
		
		$reflection = new ReflectionClass( get_called_class() );
	    try
	    {
	       	$methodref = $reflection->getMethod($methodname);
	       	return (
		       	$methodref->isPublic() // is public
		       	&&  $methodref->isFinal() // is final 
		       	&&  !$methodref->getNumberOfParameters()  // no parameter function 
		       	&& ! $methodref->hasReturnType() // has not a return type
		    );
	    }
	    catch ( ReflectionException $e) 
	    {
	    	return false;	
	    }   
	}

	Final public function __set($name,$value)
	{

		if(!in_array($name ,$this->_availablecolumns ) )
    		throw new Exception( sprintf("property '%s' dosn't exist in class %s ",$name,get_called_class() ) );

    	$oldvalue = array_key_exists($name,$this->publicvars) ? $this->publicvars[$name] : null;

    	if($oldvalue === $value )
    		return true;

    	$st = $this->attributeValue($name,$value);
		if(!$st)
    		throw new Exception(sprintf("Error setting value for property '%s' in class '%s' ",$name,get_called_class() ));
    	
	 	if( $this->timestamps )
			$this->publicvars[static::UPDATE_AT] = new DateTime();

		$this->TrackModification($name,$oldvalue,$this->publicvars[$name]);
	}

	Final public function __get($name)
	{		
		$classname = get_called_class();
		$v = $this->GetAttribute($name);
		if( $v === false )
		{
			if( $this->IsLinkMethod($name) )
			{
				$result = call_user_func_array([$this , $name ],[]);
       			if( $result instanceof OrmRelation )
       				return $result->load();
			}
    		throw new Exception( sprintf("property '%s' dosn't exist in class %s ",$name,get_called_class() ) );	
		}
		else
			return $v;
	}

	Private function Insert()
	{
		$param = [];
		$select_str = implode(",",$this->_availablecolumns);
		$values_str = [];

		foreach($this->_availablecolumns as $column)
		{
			// ------- handling PK column -----------------
			if($column === $this->getpk() )
			{
				if( static::$autoincrement  )
					$this->publicvars[ $this->getpk() ] = null;
				elseif( $this->publicvars[ $this->getpk() ] === null )
					throw new Exception(sprintf("no autoincrement PK can't be NULL in ORM class %s", get_called_class() ));
			}

			$buff = null;
			if(array_key_exists($column, $this->publicvars ))
				$buff = $this->publicvars[$column];
			elseif( array_key_exists($column, $this->attributes))
				$buff = $this->attributes[$column];
			else
				throw new Exception( sprintf("field '%s' isn't defined or don't have default fallback value in ORM Class %s",$column, get_called_class()) );

			if($buff === null)
				$values_str[] =  "NULL";
			elseif ($buff instanceof DateTime) 
				$values_str[] =  "FROM_UNIXTIME(".$buff->getTimestamp().")";
			else
			{
				$values_str[] =  "?";
				$param[] = $buff;
			}
		}

		$values_str = implode(",",$values_str);
		$sql= sprintf("INSERT INTO `%s` (%s) VALUES (%s)",$this->table() , $select_str , $values_str);
		DB::TStart();
		$req = DB::Prepare($sql,$param);

		if($req && $req->Count() )
		{	
			DB::TCommit();
			$this->attributeValue( $this->getpk() , DB::LastInsert() );
			return true;
		}
		else
			DB::TRollback();

		return false;
	}

	Private function Update()
	{
		$param = [];
		$update_str = [];
		foreach($this->_modifiedfilds as $column => $tracks )
		{
			
			// ------- handling PK column -----------------
			if($column === $this->getpk() )
				continue;

			$buff = null;
			if(array_key_exists($column, $this->publicvars ))
				$buff = $this->publicvars[$column];
			else
				return false;

			if($buff === null)
				$update_str[] =  "`{$column}` = NULL";
			elseif ($buff instanceof DateTime) 
				$update_str[] =  "`{$column}` = FROM_UNIXTIME(".$buff->getTimestamp().")";
			else
			{
				$update_str[] =  "`{$column}` = ?";
				$param[] = $buff;
			}
		}
		$param[]=$this->id(); 
		$update_str = implode(",",$update_str);

		$sql= sprintf("UPDATE `%s` SET %s WHERE %s = ?",$this->table() , $update_str , $this->getpk() );
		DB::TStart();
		$req = DB::Prepare($sql,$param);

		if($req)
		{	
			DB::TCommit();
			$this->_modifiedfilds = [];
			return true;
		}
		else
			DB::TRollback();

		return false;
	}

	static private function AllowedStatic($name)
	{

		if( preg_match('#^(or)?where$#i',$name) )
			return true;
        elseif( preg_match('#^(or)?where(null|notnull|raw)$#i',$name) )
			return true;
        elseif( preg_match('#^(or)?where(in|notin|between|notbetween)$#i',$name) )
        	return true;
        else
        	return false;
	}


// ********************************* Protected ************************
	
// -----------------   RelationShips --------------

	Private function AnalyseOrmModel($rModel)
	{
		try
		{
			$robj = new ReflectionClass($rModel);
			if(!$robj->isSubclassOf('Lfw\Database\ORM\Model'))
				return false;

			$staticPropertys = $robj->getStaticProperties();
			return $staticPropertys;
		}
		catch( ReflectionException $e)
		{
			return false;
		}
	}

	// ++++++++ One to One +++++++++++++++
		Final Protected function hasOne($rModel,$foreignkey=null,$key=null )
		{
			$classname = get_called_class();
			$metadata = $this->AnalyseOrmModel($rModel);
			if(!$metadata)
				throw new Exception(sprintf("'%s' is not a valide ORM Model in Class %s",$rModel,$classname ));
			/// getting table name for the target Model

			$rtable = ($metadata["tablename"] == null) ? strtolower(basename($rModel)) : $metadata["tablename"];

			// --- getting fields and values
			$foreignkey = ($foreignkey === null) ? $this->table()."_id" : $foreignkey;
			$key = ($key === null) ? $this->getpk() : $key;
			$idvalue = $this->GetAttribute($key);
			if($idvalue === false)
				throw new Exception(sprintf("local property '%s' dosn't exist in ORM Class %s",$key,$classname ));

			// ---- Init QueryBuilder
			
			$arg = [
					"callertable" => $this->table(),
					"foreignkey"=>$foreignkey,
					"pk" => $key,
					"pkvalue" => $idvalue
				];
			$forger = new OrmRelation($rModel,$rtable);
			$forger->SetRelation(OrmRelation::HasOne , $arg );
			
			return $forger;
		}

		Final Protected function belongsTo($rModel,$foreignkey=null,$key=null )
		{
			$classname = get_called_class();
			$metadata = $this->AnalyseOrmModel($rModel);
			if(!$metadata)
				throw new Exception(sprintf("'%s' is not a valide ORM Model in Class %s",$rModel,$classname ));
			/// getting table name for the target Model

			$rtable = ($metadata["tablename"] == null) ? strtolower(basename($rModel)) : $metadata["tablename"];

			// --- getting fields and values
			$foreignkey = ($foreignkey === null) ? "{$rtable}_id" : $foreignkey;
			$key = ($key === null ) ? $metadata['primarykey'] : $key;
			$idvalue = $this->GetAttribute($foreignkey);
			if($idvalue === false)
				throw new Exception(sprintf("local property '%s' dosn't exist in ORM Class %s",$foreignkey,$classname ));

			// ---- Init QueryBuilder
			
			$arg = [
					"callertable" => $this->table(),
					"foreignkey"=>$foreignkey,
					"pk" => $key,
					"pkvalue" => $idvalue
				];
			$forger = new OrmRelation($rModel,$rtable);
			$forger->SetRelation(OrmRelation::BelongsTo , $arg , $this);
			

			return $forger;
		}

	// ++++++++ One to Many ++++++++++++++
		Final Protected function hasMany($rModel,$foreignkey=null,$key=null )
		{
			$classname = get_called_class();
			$metadata = $this->AnalyseOrmModel($rModel);
			if(!$metadata)
				throw new Exception(sprintf("'%s' is not a valide ORM Model in Class %s",$rModel,$classname ));
			/// getting table name for the target Model

			$rtable = ( $metadata["tablename"] == null ) ? strtolower(basename($rModel)) : $metadata["tablename"];

			// --- getting fields and values
			$foreignkey = ($foreignkey === null) ? $this->table()."_id" : $foreignkey;
			$key = ($key === null) ? $this->getpk() : $key;
			$idvalue = $this->GetAttribute($key);
			if($idvalue === false)
				throw new Exception(sprintf("local property '%s' dosn't exist in ORM Class %s",$key,$classname ));
			// ---- Init QueryBuilder
			$arg = [
					"callertable" => $this->table(),
					"foreignkey"=>$foreignkey,
					"pk" => $key,
					"pkvalue" => $idvalue
				];
			$forger = new OrmRelation($rModel,$rtable);
			$forger->SetRelation(OrmRelation::HasMany , $arg );
			return $forger;
		}

	// ++++++++ Many to Many +++++++++++++

		Final Protected function belongsToMany($rModel,$pivotetable=null , $mforeignkey = null , $rforeignkey = null )
		{
			$classname = get_called_class();
			$metadata = $this->AnalyseOrmModel($rModel);
			if(!$metadata)
				throw new Exception(sprintf("'%s' is not a valide ORM Model in Class %s",$rModel,$classname ));
			
			$pkvalue = $this->id();
			
			//if($pkvalue === null)
				//return null;

			$rtable = ( $metadata["tablename"] == null ) ? strtolower(basename($rModel)) : $metadata["tablename"];
			if( $pivotetable === null )
			{	
				$buff = [ $rtable , $this->table()];
				sort($buff);
				$pivotetable = implode("_", $buff );
			}

			if( $mforeignkey === null )
				$mforeignkey = $this->table()."_id";

			if( $rforeignkey === null )
				$rforeignkey = "{$rtable}_id";

			// ---- Init QueryBuilder
			$arg = [
					"callertable" => $this->table(),
					"pivotetable"=>$pivotetable,
					"mforeignkey" => $mforeignkey,
					"rforeignkey" => $rforeignkey,
					"mpk"=> $this->getpk() ,
					"rpk"=> $metadata['primarykey'] ,
					"pkvalue" => $pkvalue
				];

			$forger = new OrmRelation($rModel,$rtable);
			$forger->SetRelation(OrmRelation::BelongsToMany , $arg );
			return $forger;
		}
		
// ************ Public Methods ****************
	Final Public function isModified()
	{

		return !empty($this->_modifiedfilds);
	}

	Final Public function isNew()
	{
		if(isset($this->publicvars[$this->getpk()]) )
		{
    		$Forger = new OrmDBForge( get_called_class() , $this->table() );
    		$Exist = $Forger->Where($this->getpk(),$this->id() )->Exist();
			return !$Exist;
		}
		else
			return true;
	}

	Final Public function getpk()
    {

    	return static::$primarykey;
    }

	Final Public function id()
    {
    	$pk =$this->getpk();
    	return $this->{$pk};
    }

  	Final Public function table()
    {

    	return static::$tablename;
    }

    Final Public function LoadByPK($pk)
    {
    	if( !static::IsPK($pk) )
    		return false;

		$Forger = new OrmDBForge( get_called_class() , $this->table() );
    	$data = $Forger->where( $this->getpk() , $pk )->One(true);
    	if(!$data || !$this->HydrateByArray($data))
    		return false;
    	return $this;
    }

    Final Public function Remove()
    {
    	$id = $this->id();
    	if( $id == null)
    		return false;

    	$Forger = new OrmDBForge( get_called_class() , $this->table() );
    	$rmv = $Forger->where($this->getpk() , $id )->delete();
    	if($rmv)
    		$this->publicvars = [];
    	return $rmv;
    }

    Final Public function Save()
    {
    	if($this->isNew())
    		$st = $this->Insert();
    	else
    		$st = $this->Update();

    	return $st;
    }

    Final Public function Reset()
    {
    	$pk = $this->id();
    	if( $pk == null )
    		return false;
    	return $this->HydrateFromDB();
    }

   	Final Private function recusiveConvert($data)
    {
    	if(is_array($data))
    		$value = array_map([$this,"recusiveConvert"], $data );
    	elseif(is_bool($data))
    		$value = intval($data);
    	elseif($data instanceof DateTime)
			$value = $data->getTimestamp();
		elseif($data instanceof Model)
			$value = $data->__toArray();
		else
			$value = $data;

		return $value;
    }

    Final public function __toArray($loadlinks = true)
    {
        $res = [];
		foreach( $this->publicvars as $field=>$value)
		{
			if(in_array($field , $this->_private))
				continue;
		
			$value = $this->recusiveConvert($value);

			$res[ $field ] = $value;
		}
		
		if(!$loadlinks)
			return $res;

		foreach( $this->displaylinks as $method)
		{
			if( $this->IsLinkMethod($method) )
			{
				$obj = call_user_func_array([$this,$method], []);
				if(!$obj)
					$value = null;
				else
					$value = $obj->load();

				$value = $this->recusiveConvert($value);

				$res[ $method ] = $value;
			}
		}

		return $res;
    }


// ************ STATIC FUNCTIONS ****************
    Final Private static function gettable()
    {
    	$table = static::$tablename;
    	if($table == null)
			$table = strtolower(basename(get_called_class()));

		return $table;
    }

    Final Public static function IsPK($pk)
    {
    	if( static::$keyisInt && !intval($pk))
    		throw new Exception( sprintf("Expecting numeric PK ! in ORM class %s",get_called_class() ) );
    	$forger = new OrmDBForge(get_called_class(),self::gettable());
    	return $forger->where(  static::$primarykey , $pk )->Exist();
    }

    Final Public static function Delete($pk)
    {
    	if( !static::IsPK($pk) )
    		return false;
    	$forger = new OrmDBForge(get_called_class(),self::gettable());
    	return $forger->where(  static::$primarykey , $pk )->Delete();
    }

    Final Public Static function PK($pk)
    {
    	if( !static::IsPK($pk) )
    		return false;
    	$forger = new OrmDBForge(get_called_class(),self::gettable());
    	return $forger->where(  static::$primarykey , $pk )->One();
    }

    Final Public Static function All()
    {
    	$forger = new OrmDBForge(get_called_class(),self::gettable());
    	return $forger->Select("*")->Many();
    }

    Final Public static function __callStatic($name, $args)
    {
    	$forger = new OrmDBForge(get_called_class(),self::gettable());
    	
        if(self::AllowedStatic($name))
        	return call_user_func_array([$forger , $name], $args );  
		
        throw new Exception(sprintf('There is no static method named "%s" in the class "%s".', $name,get_called_class()));
    }

}

?>
