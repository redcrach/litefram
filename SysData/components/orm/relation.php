<?php
namespace Lfw\Database\ORM;

use LFW\Database\DB;
use LFW\Database\ORM\OrmDBForge;

use \ReflectionProperty;
use \ReflectionMethod;
use \ReflectionClass;
use \Exception;

Class OrmRelation  extends OrmDBForge
{
	Protected 
		$loadmany = true,
		$relationdata = [];

	const 
		HasOne = 1,
		HasMany = 2,
		BelongsTo = 3,
		BelongsToMany = 4;

	Public function SetRelation($type,$relationdata,&$callerobj = null)
	{
		$this->loadmany = in_array($type, [self::HasMany ,  self::BelongsToMany]);

		if( $type == self::HasOne || $type == self::HasMany )
		{
			/* 
				select 
					table.*
				from table 
				inner join callertable on table.foreinkey = callertable.pk 
				where 
					table.foreinkey = value 
			*/

			$callertable = $relationdata['callertable'];
			$foreignkey = $relationdata['foreignkey'];
			$pk = $relationdata['pk'];
			$pkvalue = $relationdata['pkvalue'];

			$this->relationdata = [
									"type" => $type,

									"caller" => [
												"table" => $callertable,
												"pk" => $pk,
												"value" => $pkvalue
											],
									"recev" => [
													"table"=> $this->table,
													"foreignkey" => $foreignkey
												]
								];
			$this->select()
				->innerJoin($callertable , [$foreignkey => "{$callertable}.{$pk}"])
				->Where("{$callertable}.{$pk}",$pkvalue);
		}
		elseif( $type == self::BelongsTo )
		{
			/*
				select 
					table.*
				from table 
				inner join callertable on callertable.foreinkey = table.pk 
				where 
					callertable.foreinkey = value 
			*/

			$callertable = $relationdata['callertable'];
			
			$foreignkey = $relationdata['foreignkey'];
			$pk = $relationdata['pk'];
			$pkvalue = $relationdata['pkvalue'];

			$this->relationdata = [
									"type" => $type,

									"caller" => [
												"table" => $callertable,
												"foreignkey" => $foreignkey,
												"keyvalue" => $pkvalue,
												"obj" => $callerobj,
											],
									"recev" => [
													"table"=> $this->table,
													"foreignkey" => $pk
												]
								];

			$this->select()
				->innerJoin($callertable , [ $pk => "{$callertable}.{$foreignkey}"])
				->Where("{$callertable}.{$foreignkey}",$pkvalue);
		}
		elseif( $type == self::BelongsToMany )
		{
			/*
				select 
					table.*,
					pivote.*
				from table
				inner join pivote on pivote.table_id = table.key 
				inner join localt on pivote.localt_id = localt.key 
				where 
					localt.key = value 
			*/

			$callertable = $relationdata['callertable'];
			$pivotetable = $relationdata['pivotetable'];
			$mforeignkey = $relationdata['mforeignkey'];
			$rforeignkey = $relationdata['rforeignkey'];
			$mpk = $relationdata["mpk"];
			$rpk = $relationdata["rpk"];
			$pkvalue = $relationdata['pkvalue'];
			


			$this->relationdata = [
									"type" => $type,

									"caller" => [
													"table" => $callertable,
													"pk" => $mpk,
													"value" => $pkvalue,
													"obj" => $callerobj,
												],
									"recev" => [
													"table"=> $this->table,
													"pk" => $rpk
												],
									"pivote" => [
													"table" => $pivotetable,
													"caller_foreignkey" => $mforeignkey,
													"recv_foreignkey" => $rforeignkey,
												]
								];
													
			$this->select()
				->innerJoin($pivotetable , [ "{$pivotetable}.{$rforeignkey}" => $rpk ])
				->innerJoin($callertable , [ "{$pivotetable}.{$mforeignkey}" => "{$callertable}.{$mpk}" ])
				->Where("{$callertable}.{$mpk}",$pkvalue);
		}
	}

	Public function Load($array=false)
	{
		if( $this->loadmany )
			return $this->Many($array);
		else
			return $this->One($array);
	}


	Private function AttachRecord($table,$columns , $values , $param)
	{
		if( count($columns ) !=  count($values) || empty($columns)  || empty($values) )
			return false;

		$sql = sprintf("INSERT INTO %s (%s) VALUES (%s)",
															$table , 
															implode(",", $columns) ,
															implode(",", $values)
														);
		DB::TStart();
		$req = DB::Prepare($sql,$param);

		if($req && $req->Count() )
		{	
			DB::TCommit();
			return true;
		}
		else
			DB::TRollback();

		return false;
	}


	Private function DetachRecord($table,$linkfield=false , $linkid=false ,$linkedfield = false , $linkedid = false)
	{
		
    	$sql = "DELETE FROM {$table}";

    	$cond = $param = [];

    	if( $linkfield  && $linkid)
    	{
    		$param[':linkid'] = $linkid;
    		$cond[] = " {$linkfield} = :linkid ";	
    	}

    	if( $linkedfield  && $linkedid)
    	{
    		$param[':linkedid'] = $linkedid;
    		$cond[] = " {$linkedfield} = :linkedid ";	
    	}
    	

    	if(!empty($cond))
    		$sql.=" WHERE ".implode(' AND ',$cond);

		DB::TStart();
		$req = DB::Prepare($sql,$param);

		if($req && $req->Count() )
		{	
			DB::TCommit();
			return true;
		}
		else
			DB::TRollback();

		return false;
	}


	Public function Associate(Model &$ormModel)
	{
		if($this->relationdata["type"] !==  self::BelongsTo )
        	throw new Exception(sprintf('There is no method named "Associate" in the class "%s".',get_called_class() ));

    	if(!is_a($ormModel , $this->ModelName ))	
    		throw new Exception(sprintf('argument need to be a valide "%s" object in class "%s"', $this->ModelName , get_called_class() ));
										
    	if( !($this->relationdata["caller"]["obj"] instanceof Model) )
    		return false;

    	$foreignkey = $this->relationdata["caller"]["foreignkey"];

    	$this->relationdata["caller"]["obj"]->{$foreignkey}  = $ormModel->id();
    	return true;
	}

	Public function Dissociate()
	{
		if($this->relationdata["type"] !==  self::BelongsTo )
        	throw new Exception(sprintf('There is no method named "Dissociate" in the class "%s".',get_called_class() ));
    		
    	if( !($this->relationdata["caller"]["obj"] instanceof Model) )
    		return false;

    	$foreignkey = $this->relationdata["caller"]["foreignkey"];
    	$this->relationdata["caller"]["obj"]->{$foreignkey}  = null;
    	return true;
	}
	

	Final Protected function dynamicm_SaveOperation($name,$arguments)
	{

		if( !in_array($this->relationdata["type"] ,[self::HasOne, self::HasMany ,self::BelongsToMany] ) )
        	return null;

	    if( strcasecmp('save',$name) ===0 && !empty($arguments) )
        {
        	$ormModel = $arguments[0];
        	if( !($ormModel instanceof Model) || !is_a($ormModel , $this->ModelName ))	
	        	throw new Exception(sprintf('argument need to be a valide "%s" object in class "%s"', $this->ModelName , get_called_class() ));

	        if($this->relationdata["type"]  === self::BelongsToMany)
	        {
	        	$extra = ( isset($arguments[1]) && is_array($arguments[1]) ) ? $arguments[1] : [] ;
	        	return $this->attach($ormModel->id(), $extra);
	        }
	        else
	        {
		    	$foreignkey = $this->relationdata['recev']['foreignkey'];
				$ormModel->{$foreignkey} = $this->relationdata['caller']['value'];
				return $ormModel->save();
        	}
        }
        elseif( strcasecmp('saveMany',$name) ===0 && !empty($arguments) )
        {
        	$modellist = $arguments[0];
        	if(!is_array($modellist)) 
        		return;

	        $models = array_filter($modellist, function($v){
	        							return (is_a($v , $this->ModelName ));
	        					});

	        if( count($models) != count($modellist) )
	        	throw new Exception(sprintf('argument need to be a array of "%s" objects in class "%s"', $this->ModelName , get_called_class() ));

			$foreignkey = $this->relationdata['recev']['foreignkey'];
		    $extra = ( isset($arguments[1]) && is_array($arguments[1]) ) ? $arguments[1] : [] ;
	        foreach($models as $model)
	        {
	        	if($this->relationdata["type"]  === self::BelongsToMany)
		        	$this->attach($model->id(), $extra);
		        else
		        {
		        	$model->{$foreignkey} = $this->relationdata['caller']['value'];
		        	$model->save();
		        }
	        }
			return true;
        }	
	}


	Final Protected function dynamicm_CreateOperation($name,$arguments)
	{

		if( !in_array($this->relationdata["type"] ,[self::HasOne, self::HasMany ,self::BelongsToMany] ) )
        	return null;

	    if( strcasecmp('create',$name) ===0 && !empty($arguments) )
        {
        	$modeldata = $arguments[0];
        	if(!is_array($modeldata))
        		return;

        	$ormModel = $this->ModelObj->newInstanceArgs( [$modeldata] );
        	if($this->relationdata["type"]  === self::BelongsToMany)
	        {
	        	$extra = ( isset($arguments[1]) && is_array($arguments[1]) ) ? $arguments[1] : [] ;
	        	return ( $ormModel->save() && $this->attach($ormModel->id(), $extra) );
	        }
	        else
	        {
				$foreignkey = $this->relationdata['recev']['foreignkey'];
				$ormModel->{$foreignkey} = $this->relationdata['caller']['value'];
				return $ormModel->save();
			}
        }
        elseif( strcasecmp('createmany',$name) ===0 && !empty($arguments) )
        {
        	$modeldatalist = $arguments[0];
        	if(!is_array($modeldatalist))
        		return;

	    	$models = array_filter($modeldatalist, function($v){
						    							return (is_array($v));
						    					});

	    	if( count($models) != count($modeldatalist) )
	    		throw new Exception(sprintf('error argument in class "%s"', $this->ModelName , get_called_class() ));

			$foreignkey = $this->relationdata['recev']['foreignkey'];
		    $extra = ( isset($arguments[1]) && is_array($arguments[1]) ) ? $arguments[1] : [] ;

	    	foreach($models as $modelarg)
	    	{

	    		$model = $this->ModelObj->newInstanceArgs( [$modelarg] );
	    		if($this->relationdata["type"]  != self::BelongsToMany)
	    		{
	    			$model->save();
		        	$this->attach($model->id(), $extra);
	    		}
		        else
	    		{
	    			$model->{$foreignkey} = $this->relationdata['caller']['value'];
	    			$model->save();
	    		}
	    	}

			return true;
        }	
	}


	Final Protected function dynamicm_ManytoManyOperations($name,$arguments)
	{
	    if($this->relationdata["type"] !==  self::BelongsToMany )
	    	return null;

		$pivotetable = 	$this->relationdata['pivote']["table"];
		$recvCol = 	$this->relationdata['pivote']["recv_foreignkey"];
		$callerCol = 	$this->relationdata['pivote']["caller_foreignkey"];
		$linkid = 	$this->relationdata['caller']["value"];
		$match = [];
	    if( strcasecmp('attach',$name) ===0 && !empty($arguments) )
	    {
	    	$extra_rows = isset($arguments[1]) ? $arguments[1] : [];
	    	$recvid = $arguments[0];


			if( !$this->ModelName::IsPK($recvid) )
	    		throw new Exception(sprintf('Incorrect PrimaryKey ID for orm model "%s"', $this->ModelName ));

	    	if( DB::Table($pivotetable)->Where($recvCol,$recvid)->Where($callerCol,$linkid)->Exist() )
	    		return true;
	    	
	    	$columns = [$callerCol, $recvCol];
			$values = ["?","?"];
			$param =  [$linkid,$recvid];

			foreach($extra_rows as $field=>$value)
			{
				$columns[] = $field;
				if($value === null)
					$values[] = "NULL";
				elseif( is_bool($value) )
					$values[] = intval($value);
				else
				{
					$values[] = "?";
					$param[] = $value;
				}
			}

	    	return $this->AttachRecord($pivotetable,$columns , $values , $param);
	    }

	    elseif( strcasecmp('detach',$name) ===0  )
	    {

	    	if(empty($arguments))
	    		return $this->DetachRecord($pivotetable, $callerCol , $linkid);
	    	else
	    	{
	    		$recvid = $arguments[0];
				if( !$this->ModelName::IsPK( $recvid ) )
		    		throw new Exception(sprintf('Incorrect PrimaryKey ID for orm model "%s"', $this->ModelName ));
		   
	    		if( !DB::Table($pivotetable)->Where($recvCol,$recvid)->Where($callerCol,$linkid)->Exist() )
	    			return true;
	    		return $this->DetachRecord($pivotetable, $callerCol, $linkid ,$recvCol , $recvid );
	    	}
	    }

	    elseif( strcasecmp('toggle',$name) ===0  && !empty($arguments) && is_array($arguments[0]) && !empty($arguments[0]) )
	    {

    		
    		$ids = $arguments[0];
	    	$extra_rows = isset($arguments[1]) ? $arguments[1] : [];

			foreach($ids as $id)
			{
	    		if( DB::Table($pivotetable)->Where($recvCol,$id)->Where($callerCol,$linkid)->Exist() )
	    			$res = $this->DetachRecord($pivotetable, $callerCol, $linkid ,$recvCol , $id );
	    		else
	    			$res = $this->attach($id , $extra_rows);

	    		if(!$res)
	    			return false;
			}		  		

			return true;
	    }

	    elseif( preg_match('#^sync(withoutdetach)?$#i',$name,$match) && !empty($arguments) && is_array($arguments[0]) && !empty($arguments[0]) )
	    {

        	if(empty($match[1]))
    			$this->detach();

    		foreach( $arguments[0] as $key=>$value )
    		{
    			$extra = [];
    			if(is_array($value))
    			{
    				$id = $key;
    				$extra = $value;
    			}
    			else
    				$id = $value;

    			if( !$this->attach($id,$extra) )
    				return false;
    		}

    		return true;
	    }
	}



}	


?>
