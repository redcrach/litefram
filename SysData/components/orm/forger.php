<?php
namespace Lfw\Database\ORM;

use LFW\Database\DB;
use LFW\Database\DBForge;

use \ReflectionProperty;
use \ReflectionMethod;
use \ReflectionClass;
use \Exception;

Class OrmDBForge  extends DBForge
{
	Protected $ModelObj,$ModelName;

	function __construct($classname,$tablename)
	{
	 	parent::__construct($tablename);
	 	$this->ModelName = $classname;
	 	try
		{
			$this->ModelObj = new ReflectionClass($classname);
		} catch (Exception $e) {
			throw new Exception( sprintf("'%s' dosn't exist!",$classname) );	
		}
	}

	// ***** OverWrite DBForge methods *****
		Public function One($toarray=false)
		{
			$resp = parent::One();

			if($resp === false)
				return false;

			if($toarray)
				return $resp;
			
	        $Obj = $this->ModelObj->newInstanceArgs( [$resp] );
	        return $Obj;
		}

		Public function Many($toarray=false)
		{
			$resp = parent::Many();
			if($resp === false)
				return false;

			if($toarray)
				return $resp;

			$result = [];

			foreach($resp as $data)
			{
	      	  	$Obj = $this->ModelObj->newInstanceArgs( [$data] );
	      	  	$result[] = $Obj;
			}

	        return $result;
		}

}

?>
