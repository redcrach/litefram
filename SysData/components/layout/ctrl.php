<?php

namespace Lfw\layout;

use Lfw\helpers\Url;

use Lfw\Core\Transactions\Authenticator;
use LFW\Database\DB;

Abstract Class controller 
{
	// *********** Private variables ********
		Private $ctrlerBehavior=[]; // private system variable
	// *********** Sharable variables ********
		Protected
			$AppPrefix, 
			$RenderEngine; // Render engine

		
	Abstract Public Function _Init_(); // The Consctruct of all sub controllers

	function __construct(&$enginetwig,$prefix=null)
	{	            
		
		$this->RenderEngine = $enginetwig;
		$this->AppPrefix = "/".$prefix;

		$this->ctrlerBehavior = $this->_Init_();
		if( !is_array($this->ctrlerBehavior))
			$this->ctrlerBehavior = [];
	}

	Final Protected function AppRedirect($url)
	{
		Url::Redirect($this->AppPrefix."/{$url}");
	}

	// ********* Private functions *************************
		Private function AnalyseBehavior($data) : array
		{
			$result = [
						"access"=>false,
						"redirect"=>false
					];

			if( !isset($data['allow']) || !in_array($data['allow'] , ["*","@","+"]) )
				return false;

			$isloged = Authenticator::isOpen();

			switch($data['allow'])
			{
				case "*": // public access
					$result["access"] = true;
				break;

				case "+": // not loged 
					$result["access"] = !boolval($isloged);
				break;

				case "@": // loged

					$result["access"] = boolval($isloged);
					if(isset($data['group']) && $result["access"])
						$result["access"] =  $this->HasGroup($data['group']);

					if(isset($data['roles']) && $result["access"])
					{
						foreach($data['roles'] as $role)
						{
							if(!isset($role['name']))
								return false;

							$rolename = $role['name'];
							$perm = [true, true , true , true]; // LINK , ADD , UPDATE , DELETE 
							if(isset($role['perm'])  )
							{
								if(!is_array($role['perm']) || count($role['perm'])> count($perm))
									return false;

								$perm = array_map(function($v,$k) use ($perm) {
														if(array_key_exists($k, $perm))
														{
															$perm[$k] = boolval($v);
															return $perm[$k];
														}
														else
															return true;
													}, $role['perm'] , array_keys($role['perm']) );
							}

							if(!$this->HasRole($rolename,$perm))
							{

								$result["access"] = false;
								break;
							}
						}
					}
				break;
			}


			$result["redirect"] = isset($data['redirect']) ? $data['redirect'] : false; 

			return (array) $result;
		}

	// ********* Controllers can use ************************
	
		Final Protected function HasGroup($groups) : bool
		{
			if(!is_array($groups))
				$groups = [$groups];
			else
				$groups = array_values($groups);

			$id = Authenticator::isOpen();

	
			$req = DB::Table("users")
					->innerjoin("users_group",["users_group.id"=>"group_id"])
					->Select("id")
					->Where("id",$id)
					->WhereIn("users_group.name",$groups)
					->Exist();

			return $req;
		}

		Final Protected function HasRole($role,$flags=[true,true,true,true]) : bool
		{
		
			if(is_array($flags) && count($flags)==4 )
				$flags = array_map(function($v){ return intval($v); }, array_values($flags) );
			else
				return false;

			$id = Authenticator::isOpen();

			$req = DB::Table("users")
					->Select("id")
					->innerjoin("users_group_has_roles",["users_group_has_roles.group_id"=>"group_id"])
					->innerjoin("group_roles",["group_roles.id"=>"users_group_has_roles.role_id"])
					->Where("id",$id)
					->Where("group_roles.name",$role)
					->Where("users_group_has_roles.role_link",$flags[0])
					->Where("users_group_has_roles.role_add",$flags[1])
					->Where("users_group_has_roles.role_update",$flags[2])
					->Where("users_group_has_roles.role_delete",$flags[3])
					->Exist();

			return $req;
		}

		Final Protected function GetAllowedActions() : array
		{
			$result = [];

			foreach($behaviors as $action=>$behavior)
			{
				$res = $this->AnalyseBehavior($behavior);
				if($res['access'])
					$result[] = $action;				
			}

			return (array) $result;
		}


	// ******************* RUN function *****************

		Final Public function _Run_($action , $variables = [] )
		{
			$methodname = "Action_$action";
			
			if( method_exists($this,$methodname)  && isset($this->ctrlerBehavior[$action]) )
			{
				$res = $this->AnalyseBehavior($this->ctrlerBehavior[$action]);

				if($res['access'])
				{
					$result=call_user_func_array( [$this,$methodname], $variables);	
					DB::Close();
					return $result;
				}
				elseif( $res['redirect'] )
					$this->AppRedirect($res["redirect"]);
				else
					return false;
			}

			return false;
		}
}


?>
