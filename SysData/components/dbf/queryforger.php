<?php
namespace Lfw\Database;

Class QueryForger 
{
	Private 
		$table,
		$columns,
		$_ForgeSelect,
		$_ForgeANDCond,
		$_ForgeORCond,
		$_ForgeParam,
		$_ForgeJoin,
		$_ForgeJoinType,
		$_ForgeEnd,
		$_Persistent;



	function __construct($table )
	{
		$this->table = strtolower($table);

		$this->columns  = DB::Columns($this->table);


		$this->_Persistent = [
								"SELECT"=>[],
								"AND"=>[],
								"OR"=>[],
								"PARAM"=>[],
								"JOIN"=>[],
								"JOINTYPE"=>[],
								"END"=>[
											"limit" => null,
											"orderby" => null,
											"groupby"=> null
										],

							];

		$this->ResetParams();
	}




	Public function Persistent()
	{
		$this->_Persistent["SELECT"] = $this->_ForgeSelect;
		$this->_Persistent["AND"] =  $this->_ForgeANDCond;
		$this->_Persistent["OR"] =  $this->_ForgeORCond;
		$this->_Persistent["PARAM"] =  $this->_ForgeParam;
		$this->_Persistent["JOIN"] =  $this->_ForgeJoin;
		$this->_Persistent["JOINTYPE"] =  $this->_ForgeJoinType;
		$this->_Persistent["END"] =  $this->_ForgeEnd;	
	}


	Public function ResetParams($hardreset=false)
	{
		if(!$hardreset)
		{
			$this->_ForgeSelect= $this->_Persistent["SELECT"];
			$this->_ForgeANDCond= $this->_Persistent["AND"];
			$this->_ForgeORCond= $this->_Persistent["OR"];
			$this->_ForgeParam= $this->_Persistent["PARAM"];
			$this->_ForgeJoin= $this->_Persistent["JOIN"];
			$this->_ForgeJoinType= $this->_Persistent["JOINTYPE"];
			$this->_ForgeEnd= $this->_Persistent["END"];
		}
		else
		{
			$this->_ForgeSelect= [];
			$this->_ForgeANDCond= [];
			$this->_ForgeORCond= [];
			$this->_ForgeParam= [];
			$this->_ForgeJoin= [];
			$this->_ForgeJoinType= [];
			$this->_ForgeEnd= [
								"limit" => null,
								"orderby" => null,
								"groupby"=> null
							];
		}
	}

	Public function getcolumns()
	{
		return $this->columns;
	}

	Public function isColumn($col)
	{
		$match = [];
		if(preg_match("#^`?([a-z0-9_]+)`?\.([a-z0-9_-]+)$#i",$col,$match) )
		{
			$table = strtolower($match[1]);
			$column = strtolower($match[2]);
			if($table === $this->table)
				return (in_array($column , $this->columns));
			else
				return $this->isTable($table);
		}
		else
			return (in_array($col , $this->columns));
	}

	Public function isTable($tablexp)
	{
		$match = [];
		if(preg_match("#^`?([a-z0-9_]+)`? AS `?([a-z0-9_]+)`?$#i",$tablexp,$match) )
			$table = $match[1];
		else
			$table = $tablexp;

		$tableslist = DB::Query( "SHOW TABLES" );
		$tablelist = array_map( function($v){
									return $v[0];
								},$tableslist->fetchall());

		return in_array($table,$tablelist);
	}


	Public function snifftable($tablexp)
	{
		$match = [];
		if(preg_match("#^`?([a-z0-9_]+)`? AS `?([a-z0-9_]+)`?$#i",$tablexp,$match) )
			return strtolower($match[2]);
		else
			return strtolower($tablexp);
	}

	Public function AddCond($cond,$or=false)
	{
		$var = ($or) ? '_ForgeORCond' : '_ForgeANDCond';
		if(!is_array($cond))
			$this->{$var}[]=$cond;
		else
			$this->{$var}= array_merge($this->{$var},$cond);
	}

	Public function AddParam($param)
	{
		if(!is_array($param))
			$this->_ForgeParam[]=$param;
		else
			$this->_ForgeParam= array_merge($this->_ForgeParam,$param);
	}

	Public function AddEnd($key,$q)
	{
		if(!array_key_exists($key, $this->_ForgeEnd))
			throw new \Exception("Error Illegal Type '$key'! ");
		$this->_ForgeEnd[$key] = $q; 
	}

	Public function Pretty($col)
	{
		$match = [];

		if(preg_match("#^`?([a-z0-9_]+)`?\.([a-z0-9_-]+)( as ([a-z0-9_-]+) )?$#i",$col,$match) )
			return "`{$match[1]}`.`{$match[2]}`".(isset($match[3]) ? $match[3] : null );
		
		elseif(preg_match("#^`?([a-z0-9_]+)`? AS `?([a-z0-9_]+)`?$#i",$col,$match) )
			return "`{$match[1]}` AS {$match[2]}";
		else
		{
			if($this->isColumn($col))
				return "`".$this->table."`.`{$col}`";
			elseif($this->isTable($col))
				return "`{$col}`";
			else
				return $col;
		}



	}

	Public function AddSelect($select,$as=null,$erase=false)
	{
		if($erase)
			$this->_ForgeSelect = [];

		if( $this->isRawExpression($select) )
			$select = $select->getquery();
		elseif( $this->isColumn($select) )
			$select = $this->Pretty($select);
		else
			return false;

		$this->_ForgeSelect[] = $select.(( $as!=null ) ? " AS $as" : null);
		
	}


	Public function AddJoin($texp,$on,$jointype)
	{

		$table = $this->snifftable($texp);
		$condition = [];

		foreach($on as $key=>$value)
		{
			if($this->isRawExpression($value))
				$value = $value->getquery();
			else
				$value = $this->Pretty($value);

			if( !is_numeric($key))
				$condition[] = $this->Pretty($key) ."=". $value;
			else
				$condition[] = $value;
		}

		$this->_ForgeJoin[$table] = $condition;
		$this->_ForgeJoinType[] = $jointype;
	}

	Public function Compile()
	{
		if( empty($this->_ForgeSelect) )
			$select = $this->table.".*";
		else
			$select = implode(',', $this->_ForgeSelect);
		$sql = sprintf("SELECT %s FROM %s",$select,$this->table);

		if(!empty($this->_ForgeJoin) && !empty($this->_ForgeJoinType))
		{
			$i=0;
			foreach($this->_ForgeJoin as $table => $cond )
			{
				$joinq = sprintf(" %s JOIN %s ON %s ",strtoupper($this->_ForgeJoinType[$i]) , $this->Pretty($table), implode(" AND ",$cond) );
				$sql .= $joinq;
				$i++;
			}
		}

		if(!empty($this->_ForgeANDCond))
		{
			$sql.= " WHERE ".implode(" AND ",$this->_ForgeANDCond);
			if(!empty($this->_ForgeORCond))
				$sql.= " OR ".implode(" AND ",$this->_ForgeORCond);
		}
		$sql = sprintf("%s %s %s %s",$sql,$this->_ForgeEnd['groupby'],$this->_ForgeEnd['orderby'],$this->_ForgeEnd['limit'] );
		$res = ["query"=>$sql,"param"=> $this->_ForgeParam];

		$this->ResetParams();
		return $res;
	}

	Public function CompileDelete()
	{
		$sql = sprintf("DELETE FROM %s",$this->table);

		if(!empty($this->_ForgeJoin) && !empty($this->_ForgeJoinType))
		{
			$i=0;
			foreach($this->_ForgeJoin as $table => $cond )
			{
				$joinq = sprintf(" %s JOIN %s ON %s ",strtoupper($this->_ForgeJoinType[$i]) , $this->Pretty($table), implode(" AND ",$cond) );
				$sql .= $joinq;
				$i++;
			}
		}

		if(!empty($this->_ForgeANDCond))
		{
			$sql.= " WHERE ".implode(" AND ",$this->_ForgeANDCond);
			if(!empty($this->_ForgeORCond))
				$sql.= " OR ".implode(" AND ",$this->_ForgeORCond);
		}
		$sql = sprintf("%s %s %s %s",$sql,$this->_ForgeEnd['groupby'],$this->_ForgeEnd['orderby'],$this->_ForgeEnd['limit'] );
		$res = ["query"=>$sql,"param"=> $this->_ForgeParam];
		$this->ResetParams();
		return $res;
	}

	

	Public function isRawExpression($exp)
	{
		return ($exp instanceof rawquery);
	}

}

?>
