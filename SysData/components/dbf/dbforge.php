<?php
namespace Lfw\Database;

use LFW\Database\DB;

use \ReflectionProperty;
use \ReflectionMethod;
use \ReflectionClass;
use \Exception;
use \DateTime;

use Lfw\Helpers\Url;

Class DBForge 
{
	Protected $Forger,$table;

	
	function __construct($table )
	{
		$this->table = strtolower($table);
		$this->Forger = new QueryForger($this->table);
	}

// -------- WHERE conditions ---------------

	Final Protected function dynamicm_where($name,$arguments)
	{
		$match = [];
	    if( preg_match('#^(or)?where$#i',$name,$match) && !empty($arguments) )
	    {
   			$ortype = (!empty($match[1]));
       		$column = $arguments[0];
       		$operator = null;
       		if(count($arguments)>2)
        	{
	        	$value = $arguments[2];
	        	$operator = $arguments[1];
	        }
	        else
	        	$value =  isset($arguments[1]) ? $arguments[1] : null;
       		return $this->CompareConditions($column,$value , $ortype ,$operator );
       	}
        elseif( preg_match('#^(or)?where(null|notnull|raw)$#i',$name,$match) )
       	{
       		$ortype = (!empty($match[1]));
        	$operator = ( !empty($match[2]) ) ? $match[2] : null;
        	return $this->OneParamConditions($arguments[0],$operator, $ortype);
        }
        elseif( preg_match('#^(or)?where(in|notin|between|notbetween)$#i',$name,$match) )
        {
   			if(!isset($arguments[1]) || !is_array($arguments[1]) || empty($arguments[1]))
  			throw new Exception("Illegal Value ! array expected");
        	$ortype = (!empty($match[1]));
       		$column = $arguments[0];
	        $value = $arguments[1];
        	$operator = ( !empty($match[2]) ) ? $match[2] : null;
        	return $this->MultiValueConditions($column,$value , $ortype ,$operator );
       	}
      
	}

		Private function CompareConditions($column,$value , $ORtype = false ,$operator = null)
		{
			if(!$this->Forger->isRawExpression($column) && !$this->Forger->isColumn($column))
				throw new Exception(sprintf('column "%s" don\'t exist in query', $column ));
			$comp = ($operator==null) ? "=" : $operator;
	   		
	   		if(!$this->Forger->isRawExpression($column))
				$this->Forger->AddCond( $this->Forger->Pretty($column)." $comp ?",$ORtype);
			else
				$this->Forger->AddCond($column->getquery()." $comp ?",$ORtype);

			$this->Forger->AddParam($value);
			return $this;
	    }

		Private function OneParamConditions($arg,$operator , $ORtype = false )
		{
	       	switch ($operator)
	       	{
	       		case 'null':
	   				if(!$this->Forger->isRawExpression($arg))
	       				$this->Forger->AddCond($this->Forger->Pretty($arg)." IS NULL",$ORtype);
	       			else
	       				$this->Forger->AddCond($arg->getquery()." IS NULL",$ORtype);
	       		break;

	       		case "notnull":
	   				if(!$this->Forger->isRawExpression($arg))
	       				$this->Forger->AddCond($this->Forger->Pretty($arg)." IS NOT NULL",$ORtype);
	       			else
	       				$this->Forger->AddCond($arg->getquery()." IS NOT NULL",$ORtype);
	       		break;

	       		case "raw":
	       			$this->Forger->AddCond($arg,$ORtype);
	       		break;
	       	}
	       	return $this;
		}

		Private function MultiValueConditions($column,$values, $ORtype = false,$operator )
		{
			if(!$this->Forger->isRawExpression($column) && !$this->Forger->isColumn($column))
	       		throw new Exception(sprintf('column "%s" don\'t exist in query', $column ));

	       	switch ($operator)
	       	{
	   			case 'in':
	   				$in = substr( str_repeat("?,",count($values)) , 0 ,-1);
	   				if(!$this->Forger->isRawExpression($column))
	   					$q =  $this->Forger->Pretty($column)." IN ($in)";
	   				else
	   					$q =  $column->getquery()." IN ($in)";

					$this->Forger->AddParam($values);
	   			break;

	   			case 'notin':
	   				$in = substr( str_repeat("?,",count($values)) , 0 ,-1);
	   				if(!$this->Forger->isRawExpression($column))
	   					$q =  $this->Forger->Pretty($column)." NOT IN ($in)";
	   				else
	   					$q =  $column->getquery()." NOT IN ($in)";
					$this->Forger->AddParam($values);
	   			break;
	   			
	   			case 'between':
	   				if(!$this->Forger->isRawExpression($column))
	   					$q =  $this->Forger->Pretty($column)." BETWEEN ? AND ?";
	   				else
	   					$q =  $column->getquery()." BETWEEN ? AND ?";
					$this->Forger->AddParam($values);
	   			break;

	   			case 'notbetween':
	   				if(!$this->Forger->isRawExpression($column))
	   					$q =  $this->Forger->Pretty($column)." NOT BETWEEN ? AND ?";
	   				else
	   					$q =  $column->getquery()." NOT BETWEEN ? AND ?";

					$this->Forger->AddParam($values);
	   			break;
	       	}
	       	$this->Forger->AddCond($q,$ORtype);
	       	return $this;
		}

	Final Protected function dynamicm_Joins($name,$arguments)
	{
		$match = [];
        if(preg_match('#^(inner|left|right)join$#i',$name,$match) && !empty($arguments) && isset($arguments[0],$arguments[1]) )
       	{
       		$jointype = $match[1];
       		if(!$this->Forger->isTable($arguments[0]))
				throw new Exception(sprintf('expresion "%s" isn\'t a valide table value', $arguments[0] ));
			$this->Forger->AddJoin($arguments[0],$arguments[1],$jointype);
			return $this;
       	}
	}

	Final public function __call($name, $arguments)
    {
       	$name = strtolower($name);

       	$reflection = new ReflectionClass( get_called_class() );
       	

		$methods = array_filter($reflection->getMethods(ReflectionMethod::IS_PROTECTED)  , function($value){
									return (stripos($value->name, "dynamicm_") === 0);
				});

		foreach($methods as $method)
		{
        	$res = call_user_func_array([$this , $method->name], [$name, $arguments] );  
        	if($res !== null)
        		return $res;
		}

        throw new Exception(sprintf('There is no method named "%s" in the class "%s".', $name, get_called_class() ));
    }

// ----------- Ordering and filtring --------------

	Public function Limit(int $num)
	{
		$this->Forger->AddEnd("limit","LIMIT $num");
		return $this;
	}

	Public function OrderBy($arg,$DESC=true)
	{
		if(!$this->Forger->isRawExpression($arg) && !$this->Forger->isColumn($arg))
			throw new Exception(sprintf('column "%s" don\'t exist in query', $arg));

		if($this->Forger->isRawExpression($arg))
			$q = "ORDER BY ".$arg->getquery().( $DESC ? " DESC" : " ASC" );
		else
			$q = "ORDER BY ".$this->Forger->Pretty($arg)." ".( $DESC ? "DESC" : "ASC" );

		$this->Forger->AddEnd("orderby",$q);
		return $this;
	}

	Public function GroupBy($column)
	{
		if(!$this->Forger->isColumn($column))
			throw new Exception(sprintf('column "%s" don\'t exist in query', $column));
		$this->Forger->AddEnd("groupby","GROUP BY ".$this->Forger->Pretty($column));
		return $this;
	}

	Public function Select(...$columns)
	{
		if(empty($columns))
			$this->Forger->AddSelect($this->table.".*" , null , true);
		
		foreach($columns as $col)
		{

			if(is_array($col))
			{
				$key = array_keys($col)[0];
				$value = array_values($col)[0];
				$this->Forger->AddSelect($key,$value);
			}
			else
				$this->Forger->AddSelect($col);

		}

		return $this;
	}

// ------- Getting data ----


	Public function Max($column)
	{
		if(!$this->Forger->isColumn($column))
			throw new Exception(sprintf('column "%s" don\'t exist in query', $column ));
		$this->Forger->AddSelect( DB::raw("MAX(".$this->Forger->Pretty($column).")"),"max", true);
		$result = $this->One('max');
		if($result)
			return $result;
		else
			return false;	
	}

	Public function Min($column)
	{
		if(!$this->Forger->isColumn($column))
			throw new Exception(sprintf('column "%s" don\'t exist in query', $column ));
		$this->Forger->AddSelect( DB::raw("MIN(".$this->Forger->Pretty($column).")"),"min", true);
		$result = $this->One('min');
		if($result)
			return $result;
		else
			return false;	
	}

	Public function SUM($column)
	{
		if(!$this->Forger->isColumn($column))
			throw new Exception(sprintf('column "%s" don\'t exist in query', $column));

		$this->Forger->AddSelect( DB::raw("SUM(".$this->Forger->Pretty($column).")"),"sum",true);
		$result = $this->One('sum');
		if($result)
			return $result;
		else
			return false;	
	}
	
	Public function Count()
	{
		$forge = $this->Forger->Compile();
		$req = DB::Prepare($forge['query'],$forge['param']);
		if(!$req)
			return false;
		return $req->Count();
	}
	
	Public function One($col=false)
	{
		$forge = $this->Forger->Compile();
		$req = DB::Prepare($forge['query'],$forge['param']);
		if(!$req)
			return false;

		$data = $req->Get();

		return ($col) ? $data[$col] : $data;
	}

	Public function Last($col=false)
	{
		$forge = $this->Forger->Compile();
		$req = DB::Prepare($forge['query'],$forge['param']);
		if(!$req)
			return false;

		$data = end($req->GetAll());
		return ($col) ? $data[$col] : $data;
	}

	Public function Many()
	{
		$forge = $this->Forger->Compile();
		$req = DB::Prepare($forge['query'],$forge['param']);
		if(!$req)
			return false;
		return $req->GetAll();
	}


	
	Public function Exist()
	{
		$forge = $this->Forger->Compile();
		
		$req = DB::Prepare($forge['query'],$forge['param']);
		if(!$req)
			return false;

		return ($req->status() && $req->Count() );
	}

	Public function Delete()
	{
		$forge = $this->Forger->CompileDelete();
		$req = DB::Prepare($forge['query'],$forge['param'],true);
		return boolval($req);
	}

}

?>
