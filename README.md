LiteFram is a simple php/mysql MVC framework !

we are using :
- simple custom made ORM and PDO 
- Symfony Twig as template engine 

By Default LiteFram comes with :

- backend ( basic CMS based on AdminLte ) 
	* default login/pass is "root"/"toor"
	* access web link is "/panel/"
- frontend ( with a basic hello world tempalte ) 

- Web API 
	* request link "/api/"

#################### Getting Started ########################

* up the database.sql file to your Database 
* edit the Config/config.php 
* Config/appmanager.php is the configuration file for the apps available
* the Apps are located in Data/Apps with a MVC architecture 

#################### URL Manager Syntex ########################

Start Tag '<'

Target :
	'?' for ctrl
	'!' for action
	':foo' to define variable 'foo'
	'@' void expression 

Filter :

	Regex		[
					/w word alphanumeric 
					/d decimal (numeric)
					/n name ( [a-zA-Z] ) 
					/f everything except '.'
					/a everything except spaces 
				]

	Aliases		{ firstAlias , secondAlias , ... }

End Tag '>'

Exemples :
<:id [/d]>
<! {add,edit,list}>
