<?php

	return [
			"frontend" =>   [
							"enabled"=>true,
							"folder"=>"main",
							"version"=>"1.0",
							"twig"=>true
						] ,
			"backend"=>	[
							"enabled"=>true,
							"version"=>"1.0",
							"prefix" => "panel",
							"twig"=>true
						],
			"api"  =>	[
						"enabled"=>false,
						"version"=>"1.0",
						"prefix" => "api",
						"twig"=>false
					],
												
		];
?>
