<?php
namespace Common;
use Lfw\Database\ORM\Model;

Class Category extends Model
{
	Protected Static $tablename = "category";
	Protected $timestamps = false;
}

?>