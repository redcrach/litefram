<?php
namespace Common;
use Lfw\Database\ORM\Model;

use Lfw\Core\Transactions\Authenticator;

use LFW\Helpers\Tools;
use LFW\Helpers\Secure;
use LFW\Database\DB;

Class User extends Model
{
	Protected Static $tablename = "users";
	Protected 
		$_private = ["password"],
		$displaylinks = ["articles"];
	// ------------- Action Methods -------------
	Public Function Login($id,$rawpassword,$remember=false)
	{
		$password = Secure::Hash($rawpassword);
		$User = User::PK($id);
		if(!$User)
			return false;
		if( $User->password === $password )
			Authenticator::SetSession($id,$remember);
		else
			return false;

		return $id;
	}

	Public function Logout()
	{
		$id = Authenticator::isOpen();
		if(!$id)
			return false;

		Authenticator::Close();

		return true;
	}
	
	// --- Static functions ---------
	Public Static function isLoged()
	{
		return Authenticator::isOpen();
	}

	Final Public function articles()
	{
		return $this->HasMany("Common\Articles","author_id");
	}
}	

?>