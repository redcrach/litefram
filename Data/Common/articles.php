<?php
namespace Common;
use Lfw\Database\ORM\Model;
Class Articles extends Model
{
	Protected Static $tablename = "articles";
	//Protected $displaylinks = ["category"];
	
	Final Public function author()
	{
		return $this->belongsTo('Common\User','author_id');
	}

	Final Public function category()
	{
		return $this->belongsToMany('Common\Category');

	}
}

?>