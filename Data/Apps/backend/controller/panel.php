<?php
namespace App\backend\controller;
use App\backend\model\PanelModel;

use Lfw\layout\controller;
use LFW\Helpers\Secure;
use LFW\Helpers\Requester;

use Common\User;


Class Panel extends controller
{
	Private $isloged = false;
	Private $User = false;
	Public Function _Init_()
	{
		$this->User = new User();

		$this->isloged = User::isLoged();

		return [
					"index" => [
									"allow" => '*'
								],
					"login" => [
								"allow" => '+',
								"redirect"=>'home'
							],
					"home" => [
								"allow" => '@',
								"redirect"=>'login'
							],			
					"logout" => [
								"allow" => '@',
								"redirect"=>'login'
							],	
				];
	}


	Public function Action_Index()
	{
		if(!$this->isloged)
			$this->AppRedirect("login");
		else
			$this->AppRedirect("home");
	}

	Public function Action_Home()
	{
		PanelModel::$selected = ["main"=>'home' , "sub"=>null ];
		PanelModel::$title = "Home";
		PanelModel::$page = "home";

		$user = ["username"=>"Admin","lastloged"=>time()];

		return $this->RenderEngine->Render("index.html",PanelModel::Render(["User"=>$user]) );
	}

	Public function Action_Login()
	{
		$error = null;
		$username = Requester::Post("user");
		$password = Requester::Post("pass");
		
		if($username !== false && $password !== false)
		{
			//var_dump(User::FindByusername($username,"LIKE"));
			$user = User::Where('username','LIKE',$username)->ORwhere('email','LIKE',$username)->One();
			if( $user !== false )
			{
				if($this->User->Login($user->id(), $password ))
					$this->AppRedirect("home");
				else
					$error="Incorrect data";
			}
		}
		return $this->RenderEngine->Render("login.html",["error"=>$error]);
	}

	Public function Action_Logout()
	{
		if($this->User->Logout())
			$this->AppRedirect("login");
		else
			$this->AppRedirect("home");
	}

}

?>