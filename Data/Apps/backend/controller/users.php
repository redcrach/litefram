<?php
namespace App\backend\controller;
use App\backend\model\PanelModel;

use Lfw\layout\controller;
use LFW\Helpers\Secure;
use LFW\Helpers\Requester;
use Lfw\Helpers\Url;

use Common\user;

Class Users extends controller
{
	Public Function _Init_()
	{
		
		return [
					"list" => [
								"allow" => '@'
							],			
					"details" => [
								"allow" => '@'
							],	
				];
	}


	Public function Action_List()
	{
		// -- set static page variables like title and view file ---
		PanelModel::$selected = ["main"=>'users' , "sub"=>'list' ];
		PanelModel::$title = "Users List";
		PanelModel::$page = "users";

		$RenderParam = [];
	
		// --------- get Articles  
		$result = [];
		$data = User::All() ;
		if($data)
		{
			foreach($data as $user)
				$result[]=$user->__toArray(false);
		}

		$RenderParam["data"]=$result;
		return $this->RenderEngine->Render("index.html",
										PanelModel::Render($RenderParam) );
	}

	Public function Action_Details($id)
	{
		PanelModel::$selected = ["main"=>'users' , "sub"=>'list' ];
		PanelModel::$title = "Users Details";
		PanelModel::$page = "userdetails";
 
		$user = User::PK($id);
		if(!$user)
			$this->AppRedirect("home");

		$data = [	
					"user"=>$user->__toArray()
		 		];
		return $this->RenderEngine->Render("index.html",PanelModel::Render( $data ) );
	}


}

?>