<?php
namespace App\backend\controller;
use App\backend\model\PanelModel;

use Lfw\layout\controller;
use LFW\Helpers\Secure;
use LFW\Helpers\Requester;
use Lfw\Helpers\Url;

use Common\articles;
use Common\category;

use Lfw\Core\Transactions\Authenticator;


Class Article extends controller
{
	Public Function _Init_()
	{
		
		return [
					"list" => [
								"allow" => '@',
								"redirect"=>'login'
							],			
					"add" => [
								
								"allow" => '@',
								/*
								"group" => ['system','author'],
								"roles"=> [
											[
												"name"=>"test",
												"perm"=>[
															false, // link 
															true, // add
															true, // edit
															true // delete 
														]
											]
										],
								*/
								"redirect"=>'home'	
							],	

					"edit" => [
								"allow" => '@',
								"group" => ['system','author'],
								"redirect"=>'home'	
							],	
					"view" => [
								"allow" => '@',
								"group" => ['system','author'],
								"redirect"=>'home'	
							],	
				];
	}


	Public function Action_List($delid=false)
	{
		// -- set static page variables like title and view file ---
		PanelModel::$selected = ["main"=>'articles' , "sub"=>"list" ];
		PanelModel::$title = "Articles List";
		PanelModel::$page = "articlelist";
		$RenderParam = [];

		// --------- delete action 
		if($delid)
		{
			$delstatus = Articles::Delete($delid);
			if($delstatus)
				PanelModel::Notify($delstatus,"Success" ,"Article deleted.");
			else
				PanelModel::Notify($delstatus,"Error Deleting" , "Illegal article id ,or article dosn't exist.");
		}
		
		// --------- get Articles  
		$result = [];
		$data = Articles::All() ; // get all articles ,with return as array 
		if($data)
		{
			foreach($data as $article)
			{
				$buff = $article->__toArray();
				$buff['author'] = $article->author->__toArray();
				$result[]= $buff;
			}
		}

		$RenderParam["data"]=$result;
		return $this->RenderEngine->Render("index.html",
										PanelModel::Render($RenderParam) );
	}

	Public function Action_Add()
	{
		PanelModel::$selected = ["main"=>'articles' , "sub"=>"add" ];
		PanelModel::$title = "Articles Add";
		PanelModel::$page = "articleadd";


		$title = Requester::Post("title");
		$body = Requester::Post("body");

		if($title && $body)
		{
			$article = new Articles(['title'=>$title,'body'=>$body]);
			$author  = \Common\User::PK(Authenticator::isOpen());
			$article->author()->associate( $author );

			if($article->Save())
				PanelModel::Notify(true,"Success" , "Article Added.");
			else
				PanelModel::Notify(false,"Error" , "Article can't be added ,please contact web admin.");
		}
		
		return $this->RenderEngine->Render("index.html",PanelModel::Render() );
	}

	Public function Action_Edit($id)
	{
		PanelModel::$selected = ["main"=>'articles' , "sub"=>"list" ];
		PanelModel::$title = "Articles Edit";
		PanelModel::$page = "articleedit";
 		$modified = null;

		$article = Articles::PK($id);
		if(!$article)
			$this->AppRedirect("home");

		$categorys = Requester::Post("categorys");

		if($categorys && is_array($categorys))
			$modified = $article->category()->Sync($categorys);

		$title = Requester::Post("title");
		$body = Requester::Post("body");
		
		if($title && !empty($title) )
			$article->title = $title;
		if($body && !empty($body) )
			$article->body = $body;

		if($article->isModified())
			$modified = $article->Save();

		if( is_bool($modified) )
		{
			if($modified)
				PanelModel::Notify(true,"Success" , "Article Updated.");
			else
				PanelModel::Notify(false,"Error" , "Article can't be updated ,please contact web admin.");
		}

		$article = Articles::PK($id);
		$cat_list = [];

		foreach(Category::All() as $cat)
			$cat_list[] = $cat->__toArray();

		$used_list = [];
		foreach( $article->category as $used )
			$used_list[] = $used->id();

		return $this->RenderEngine->Render("index.html",PanelModel::Render( [
																				"article"=> $article->__toArray(),
																				"used_cat" => $used_list,
																				"categorys" => $cat_list
																			] ) );
	}

	Public function Action_View($id)
	{
		PanelModel::$selected = ["main"=>'articles' , "sub"=>"list" ];
		PanelModel::$title = "Article #{$id}";
		PanelModel::$page = "articleview";
 
		$article = Articles::PK($id);
		if(!$article)
			$this->AppRedirect("home");
		
		$data = $article->__toArray();

		foreach( $article->category as $cat)
			$data['category'][] = $cat->__toArray();

		return $this->RenderEngine->Render("index.html",PanelModel::Render( ["article"=> $data] ) );
	}

}

?>