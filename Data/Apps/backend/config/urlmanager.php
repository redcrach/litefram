<?php

	return [	
				[
					"ctrl"   => "panel",
					"action" => "index"
				],
				[
					"url"  => "<! {login,home,logout}>",
					"ctrl" => "panel",
				],

				[
					"url" => "users" ,  
					"ctrl"=>"users",
					"action"=>"list"
				],
				[
					"url" => "users/<:id [/d]>" ,  
					"ctrl"=>"users",
					"action"=>"details"
				],

				[
					"url" => "articles/<! {list,add}>" ,  
					"ctrl"=>"article"
				],
				[
					"url" => "articles/r/<:delid [/d]>" ,  
					"ctrl"=>"article",
					"action"=>"list"
				],
				[
					"url" => "articles/<! {edit,view}>/<:id [/d]>" ,  
					"ctrl"=>"article"
				]

			];
?>