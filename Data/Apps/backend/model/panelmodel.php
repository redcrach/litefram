<?php
namespace App\backend\model;
use Lfw\Helpers\Url;

Class PanelModel
{
	Public static
		$title = null,
		$selected = [],
		$panelpath = "panel",
		$page = null;

	Private static $Notify = null;
	

	Public static function Notify($status,$maintxt,$subtxt=null)
	{
		self::$Notify = [
							"type"=>boolval($status),
							"text"=> ["main"=>$maintxt , "sub"=>$subtxt]
						];
	}

	Public static function Render($params=[])
	{
		$data = [	
					'selected' => self::$selected,
					'title' => self::$title,
					'panelpath' => self::$panelpath,
					'includepage' => self::$page,
				];

		if( is_array(self::$Notify) )
			$data["notify"] = self::$Notify;

		return array_merge($data,$params);
	}	




}

?>