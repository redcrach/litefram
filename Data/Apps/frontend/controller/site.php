<?php
namespace App\frontend\controller;
use Lfw\layout\controller;
use LFW\Helpers\Secure;
use LFW\Helpers\Url;

use Common\Articles;
use LFW\Database\DB;

Class Site extends controller
{
	Public Function _Init_()
	{
		return [
				"index"=>[
							'allow'=>'*'
						],
				"blog"=>[
							'allow'=>'*'
						],
				"content"=>[
							'allow'=>'*'
						],
			];
	}

	Public function Action_Index()
	{

		$res = DB::Table("articles")
					->innerjoin("users",["users.id"=>"author_id"])
					->Select("body","title",["users.username" => "author"])
					->WhereBetween("id",[1,10])
					->One();

		return $this->RenderEngine->Render("index.html",["extra"=>$res]);
	}

	Public function Action_Blog()
	{
		$articles =new Articles();
		return $this->RenderEngine->Render("blog.html",["articles"=>$articles->list()]);
	}


	Public function Action_Content($id)
	{
		if(!$id)
			Url::Redirect("/");

		$articles = new Articles();
		return $this->RenderEngine->Render("content.html",["data"=>$articles->get($id)]);
	}

}
?>