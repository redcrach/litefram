<?php

	return [	
				[
					"ctrl"=>"site",
					"action"=>"index"
				],
				[
					"url" => "blog",
					"ctrl"=>"site",
					"action"=>"blog"
				],
				[
					"url" => "article/<:id [/d]>",
					"ctrl" => "site",
					"action" => "content"
				],
		];
?>